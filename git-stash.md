- [ ] How to create a stash
```
git stash
git stash -a
```
 - [ ] List your stashes
```
git stash list
```
- [ ] Add description to stash
```
git stash save "Added"
```
- [ ] Retrieving stashed changes
```
git stash pop stash@{1}
git stash apply stash@{1}
```
- [ ] Cleaning up the stash
```
git stash clear
git stash drop <stash id>
```
